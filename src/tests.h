#ifndef LAB2_TESTS_H
#define LAB2_TESTS_H
#pragma once

#include <string.h>
#include <stdbool.h>
#include "util.h"

enum test_status {
    TEST_OK = 0,
    TEST_ERROR
};
struct test_struct{
    size_t size;
    bool is_free;
};

static const char* const test_msg[] = {
        [TEST_OK] = " passed\n",
        [TEST_ERROR] = " didn't pass\n"
};

enum test_status allocation_memory();
enum test_status free_one_block_of_plenty_blocks();
enum test_status free_two_blocks_of_plenty_blocks();
enum test_status memory_ends_heap_grows();
enum test_status memory_ends_heap_grows_in_new_region();
void run();
#endif //LAB2_TESTS_H
