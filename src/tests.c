#include "mem_internals.h"
#include "tests.h"
#include "mem.h"
#include <stdio.h>
#include <string.h>


enum test_status allocation_memory() {
    size_t size = 30;
    enum test_status state = TEST_ERROR;
    struct test_struct* test = _malloc(size);
    struct block_header* block = (struct block_header*){HEAP_START};
    debug_heap(stdout, HEAP_START);
    if(!block->is_free && block->next != NULL && block->next->is_free && block->capacity.bytes == size){
        state = TEST_OK;
    }
    _free(test);
    if(block == NULL || !block->is_free || block->next != NULL){
        state = TEST_ERROR;
    }
    debug_heap(stdout, HEAP_START);
    printf("%s", "Allocation memory test");
    return state;
}

enum test_status free_one_block_of_plenty_blocks(){
    enum test_status state = TEST_ERROR;
    size_t size1 = 30;
    size_t size2 = 1;
    struct test_struct* test1 = _malloc(size1);
    test1->size = size2;
    struct test_struct* test2 = _malloc(size2);
    struct block_header* block = (struct block_header*){HEAP_START};
    debug_heap(stdout, HEAP_START);
    if(block != NULL && !block->is_free && block->next != NULL && block->next->next != NULL && !block->next->is_free &&
    block->next->next->is_free && block->capacity.bytes == size1){
        state = TEST_OK;
    }
    _free(test2);
    debug_heap(stdout, HEAP_START);
    if(block == NULL || block->next == NULL || block->next->next != NULL || block->is_free || !block->next->is_free){
        state = TEST_ERROR;
    }
    _free(test1);
    debug_heap(stdout, HEAP_START);
    if(block == NULL || block->next != NULL || !block->is_free ){
        state = TEST_ERROR;
    }
    printf("%s", "Free one block of plenty blocks");
    return state;
}

enum test_status free_two_blocks_of_plenty_blocks() {
    enum test_status state = TEST_ERROR;
    size_t size1 = 24;
    size_t size2 = 27;
    size_t size3 = 30;
    struct test_struct* test1 = _malloc(size1);
    struct test_struct* test2 = _malloc(size2);
    struct test_struct* test3 = _malloc(size3);
    test3->size = 0;
    struct block_header* block = (struct block_header*){HEAP_START};
    debug_heap(stdout, HEAP_START);
    if(block != NULL && !block->is_free && block->next != NULL && block->next->next != NULL && !block->next->is_free &&
       !block->next->next->is_free && block->next->capacity.bytes == size2){
        state = TEST_OK;
    }
    _free(test2);
    debug_heap(stdout, HEAP_START);
    if(block == NULL || block->next == NULL || block->is_free || !block->next->is_free || block->next->next->next->next != NULL){
        state = TEST_ERROR;
    }
    _free(test1);
    debug_heap(stdout, HEAP_START);
    if(block == NULL || block->next == NULL || block->next->next->next != NULL || !block->is_free || block->next->is_free ||
    block->capacity.bytes != size_from_capacity((block_capacity){size2 + size1}).bytes){
        state = TEST_ERROR;
    }
    _free(test3);
    debug_heap(stdout, HEAP_START);
    printf("%s", "Free two block of plenty blocks");
    return state;
}

enum test_status memory_ends_heap_grows() {
    enum test_status state = TEST_ERROR;
    size_t size = 9000;
    struct test_struct* test = _malloc(size);
    debug_heap(stdout, HEAP_START);
    struct block_header* block = (struct block_header*){HEAP_START};
    if(block != NULL && !block->is_free && block->capacity.bytes == size && block->next->is_free) {
        state = TEST_OK;
    }
    _free(test);
    debug_heap(stdout, HEAP_START);
    printf("%s", "Memory ends heap grows blocks");
    return state;
}

enum test_status memory_ends_heap_grows_in_new_region() {
    enum test_status state = TEST_ERROR;
    size_t size = 20000;
    struct test_struct* test1 = _malloc(size);
    struct test_struct* test2 = _malloc(size);
    debug_heap(stdout, HEAP_START);
    struct block_header* block = (struct block_header*){HEAP_START};
    if(block != NULL && !block->is_free && block->capacity.bytes == size) {
        state = TEST_OK;
    }
    _free(test2);
    debug_heap(stdout, HEAP_START);
    _free(test1);
    debug_heap(stdout, HEAP_START);
    printf("%s", "Memory ends heap grows in new region blocks");
    return state;
}

void run() {
    heap_init(2288);
    enum test_status state = allocation_memory();
    printf("%s \n", test_msg[state]);
    state = free_one_block_of_plenty_blocks();
    printf("%s \n", test_msg[state]);
    state = free_two_blocks_of_plenty_blocks();
    printf("%s \n", test_msg[state]);
    state = memory_ends_heap_grows();
    printf("%s \n", test_msg[state]);
    state = memory_ends_heap_grows_in_new_region();
    printf("%s \n", test_msg[state]);
}

