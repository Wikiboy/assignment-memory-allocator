#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
    *((struct block_header*)addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );

static size_t block_actual_capacity( size_t query ) { return size_max( query, BLOCK_MIN_CAPACITY ); }

static void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
    struct region new_region = REGION_INVALID;
    size_t size_of_block = region_actual_size(query);
    void* region_addr = map_pages(addr, size_of_block, MAP_FIXED);
    bool extends = true;
    if(region_addr == MAP_FAILED){
        region_addr = map_pages(addr, size_of_block, 0);
        extends = false;
        if(region_addr == MAP_FAILED){
            return new_region;
        }
    }
    new_region.addr = region_addr;
    new_region.size = query;
    new_region.extends = extends;
    block_init(region_addr, (block_size){size_of_block}, NULL);
    return new_region;
}

static void* block_after( struct block_header const* block );

void* heap_init( size_t initial ) {
    const struct region region = alloc_region( HEAP_START, initial );
    if ( region_is_invalid(&region) ) return NULL;

    return region.addr;
}


/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
    return block-> is_free && (size_from_capacity((block_capacity){query})).bytes + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
    if(block != NULL){
        size_t capacity = block_actual_capacity(query);
        if(block_splittable(block, capacity)){
            block_init((uint8_t*)block + size_from_capacity((block_capacity){capacity}).bytes,
                       size_from_capacity((block_capacity){block->capacity.bytes - size_from_capacity((block_capacity){capacity}).bytes}), block->next);
            block->capacity.bytes = capacity;
            block->next = (struct block_header*)((uint8_t*)block + size_from_capacity((block_capacity){capacity}).bytes);
            return true;
        }
    }
    return false;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block ) {
    return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (struct block_header const* fst, struct block_header const* snd ) {
    return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ){

    if(block != NULL && block->next != NULL){
        if(mergeable(block, block->next)){
            struct block_header* next_block = block->next;
            if(blocks_continuous(block, block->next)){
                size_t new_capacity = block->capacity.bytes + size_from_capacity(block->next->capacity).bytes;
                block->capacity.bytes = new_capacity;
                block->next = next_block->next;
                return true;
            }
        }
    }
    return false;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
    struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
    struct block_header*  new_block = block;
    struct block_header*  new_block_not_null = new_block;
    if(new_block == NULL){
        return (struct block_search_result) {BSR_CORRUPTED, new_block_not_null};
    }
    while (new_block != NULL) {
        if (new_block->is_free) {
            if (block_is_big_enough(sz, new_block)) {
                new_block_not_null = new_block;
                return (struct block_search_result) {BSR_FOUND_GOOD_BLOCK, new_block_not_null};
            } else {
                if(!try_merge_with_next(new_block)){
                    new_block_not_null = new_block;
                    new_block = new_block -> next;
                }
            }
        }
        else{
            new_block_not_null = new_block;
            new_block = new_block -> next;
        }
    }
    return (struct block_search_result) {BSR_REACHED_END_NOT_FOUND, new_block_not_null};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    struct block_search_result found_block = find_good_or_last(block, query);
    if(found_block.type == BSR_FOUND_GOOD_BLOCK){
        split_if_too_big(found_block.block, query);
    }
    return found_block;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    struct region new_region = alloc_region((uint8_t*)last + size_from_capacity(last->capacity).bytes, query);
    last->next = new_region.addr;
    return new_region.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
    struct block_header* block = heap_start;
    struct region new_region = REGION_INVALID;
    struct block_search_result found_block = try_memalloc_existing(query, block);
    if(found_block.type == BSR_REACHED_END_NOT_FOUND){
        if(found_block.block->is_free) {
            block = grow_heap(found_block.block, (size_from_capacity((block_capacity){query})).bytes - (size_from_capacity(found_block.block->capacity)).bytes);
            found_block.block->next = block;
            found_block = try_memalloc_existing(query, heap_start);
        }
        else{
            block = grow_heap(found_block.block, query);
            found_block = try_memalloc_existing(query, block);
        }
    }else if(found_block.type == BSR_CORRUPTED){
        new_region = alloc_region(found_block.block, query);
        found_block = try_memalloc_existing(query, new_region.addr);
    }
    return found_block.block;
}

void* _malloc( size_t query ) {
    struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
    if (addr) {
        addr->is_free = false;
        return addr->contents;
    }
    else return NULL;
}

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
    if (!mem) return ;
    struct block_header* header = block_get_header( mem );
    header->is_free = true;
    while (try_merge_with_next(header));
}
